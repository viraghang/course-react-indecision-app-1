// arguments object - no longer bound with arrow functions
// this keyword - no longer bound with arrow functions

const add = (a, b) => {
  // console.log(arguments)
  return a + b
}

console.log(add(55, 1))

const user = {
  name: 'Zoli',
  cities: ['Bp', 'Godollo', 'Gyongyos'],
  printPlacesLived () {
    return this.cities.map((city, index) => (index + 1) + '. ' + this.name + ' has lived in ' + city).join('\n')
  }
}

console.log(user.printPlacesLived())

const multiplier = {
  numbers: [4, 5, 6, 7],
  multiplyBy: 9,
  multiply () {
    return this.numbers.map(number => number * this.multiplyBy)
  }
}

console.log(multiplier.multiply())
