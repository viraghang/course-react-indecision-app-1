import sub, {square as sq, add} from './utils.js'
import isSenior, {isAdult, canDrink} from './person.js'

console.log(sq(18))
console.log(add(18, 99))
console.log(sub(18, 99))

console.log(isAdult(18))
console.log(canDrink(18))
console.log(isSenior(18))
