/* globals React, ReactDOM */
class ToggleVisibility extends React.Component {
  constructor (props) {
    super(props)
    this.handleToggle = this.handleToggle.bind(this)
    this.state = {
      visibility: false
    }
  }
  handleToggle () {
    this.setState(prevState => ({visibility: !prevState.visibility}))
  }
  render () {
    return (
      <div>
        <h1>Visibility Toggle</h1>
        <button type='button' onClick={this.handleToggle}>{this.state.visibility ? 'Hide Details' : 'Show Details'}</button>
        {this.state.visibility && <p>Here are some further details for you.</p>}
      </div>
    )
  }
}

ReactDOM.render(<ToggleVisibility />, document.getElementById('app'))

// const appRoot = document.getElementById('app')

// let visible = false

// const onToggleVisibility = () => {
//   visible = !visible
//   render()
// }

// const render = () => {
//   const template = (
//     <div key='visibilityToggle'>
//       <h1>Visibility Toggle</h1>
//       <button type='button' onClick={onToggleVisibility}>{visible ? 'Hide Details' : 'Show Details'}</button>
//       {visible && <p>Here are further details for you</p>}
//     </div>
//   )
//   ReactDOM.render(template, appRoot)
// }

// render()
