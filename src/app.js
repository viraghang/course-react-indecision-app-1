/* Third-party libraries */
import React from 'react'
import ReactDOM from 'react-dom'
import IndecisionApp from './components/indecision-app'
import 'normalize.css/normalize.css'
import './styles/styles.styl'

// Rendering to the DOM
ReactDOM.render(<IndecisionApp />, document.getElementById('app'))
