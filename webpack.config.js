const path = require('path')

module.exports = {
  mode: 'production',
  entry: ['babel-polyfill', './src/app.js'],
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [{
      loader: 'babel-loader',
      test: /\.js$/,
      exclude: /node_modules/
    },
    {
      test: /\.styl$|\.css$/,
      use: [
        {loader: 'style-loader', options: {sourceMap: true}},
        {loader: 'css-loader', options: {sourceMap: true}},
        {loader: 'stylus-loader', options: {sourceMap: true}}
      ]
    }]
  },
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    contentBase: path.resolve(__dirname, 'public'),
    compress: true,
    port: 8080,
    watchContentBase: true,
    watchOptions: {
      poll: true
    }
  }
}
